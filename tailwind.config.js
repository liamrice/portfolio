  // tailwind.config.js
  module.exports = {
        purge: [],
        purge: ['./src/**/*.{js,jsx,ts,tsx}'],
        darkMode: 'class', // or 'media' or 'class'
        theme: {
          extend: {
            fontFamily: {
              'body': ['Circular Book', 'Helvetica', 'Arial', 'sans-serif'],
              'header': ['Circular Bold', 'Helvetica', 'Arial', 'sans-serif'],

            },
            // Adds a new breakpoint in addition to the default breakpoints
            screens: {
              '8xl': '90vw',
            },
          },
        },
        variants: {
          extend: {
            
          },
        },
        plugins: [],
      }