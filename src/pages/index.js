import * as React from "react";
import { Fragment } from "react";
import { Disclosure, Menu, Transition } from "@headlessui/react";
import { BellIcon, MenuIcon, XIcon } from "@heroicons/react/outline";
import ThemeToggle from "../components/themeToggle";
import { Link } from "gatsby";
import {Animated} from "react-animated-css";

const navigation = ["LinkedIn", "Behance", "Dribbble", "Medium"];

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

// markup
/* This example requires Tailwind CSS v2.0+ */

export default function Example() {
  return (
    
    <div className="bg-white dark:bg-gray-800">
      <Disclosure as="nav" className="">
        {({ open }) => (
          <>
            <div className="max-w-screen-9xl mx-auto px-6 lg:px-8">
              <div className="flex items-center justify-between h-24">
                <div className="flex items-center">
                  <div className="flex-shrink-0">
                    <img
                      className="h-8 w-8"
                      src="https://tailwindui.com/img/logos/workflow-mark-indigo-500.svg"
                      alt="Workflow"
                    />
                  </div>
                  <div className="hidden md:block">
                    <div className="ml-10 flex items-baseline space-x-4">
                      <a
                        href="#"
                        className="text-gray-300 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-sm font-medium"
                      >
                        LinkedIn
                      </a>
                      <a
                        href="#"
                        className="text-gray-300 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-sm font-medium"
                      >
                        Dribbble
                      </a>
                      <a
                        href="#"
                        className="text-gray-300 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-sm font-medium"
                      >
                        Medium
                      </a>
                    </div>
                  </div>
                </div>
                <div className="hidden md:block">
                  <div className="ml-4 flex items-center md:ml-6">
                    <div className="flex items-center px-4 ">
                      <ThemeToggle />
                      <label
                        for="toggle"
                        class="text-xs text-gray-700 dark:text-yellow-500"
                      >
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          className="h-6 w-6"
                          fill="none"
                          viewBox="0 0 24 24"
                          stroke="currentColor"
                        >
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            strokeWidth={2}
                            d="M9.663 17h4.673M12 3v1m6.364 1.636l-.707.707M21 12h-1M4 12H3m3.343-5.657l-.707-.707m2.828 9.9a5 5 0 117.072 0l-.548.547A3.374 3.374 0 0014 18.469V19a2 2 0 11-4 0v-.531c0-.895-.356-1.754-.988-2.386l-.548-.547z"
                          />
                        </svg>
                      </label>
                    </div>
                    <div className="px-4">
                    <button class="bg-blue-700 hover:bg-blue-700 text-gray-50 hover:text-gray-50 font-medium py-4 px-6 rounded flex ">
                    Get in touch
                    <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 ml-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
  <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M8 12h.01M12 12h.01M16 12h.01M21 12c0 4.418-4.03 8-9 8a9.863 9.863 0 01-4.255-.949L3 20l1.395-3.72C3.512 15.042 3 13.574 3 12c0-4.418 4.03-8 9-8s9 3.582 9 8z" />
</svg>
                  </button>
                    </div>
                  </div>
                </div>
                <div className="-mr-2 flex md:hidden justify-center items-center">
                <div className="flex  w-full px-4 ">
                      <ThemeToggle />
                      <label
                        for="toggle"
                        class="text-xs text-gray-700 dark:text-yellow-500"
                      >
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          className="h-6 w-6"
                          fill="none"
                          viewBox="0 0 24 24"
                          stroke="currentColor"
                        >
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            strokeWidth={2}
                            d="M9.663 17h4.673M12 3v1m6.364 1.636l-.707.707M21 12h-1M4 12H3m3.343-5.657l-.707-.707m2.828 9.9a5 5 0 117.072 0l-.548.547A3.374 3.374 0 0014 18.469V19a2 2 0 11-4 0v-.531c0-.895-.356-1.754-.988-2.386l-.548-.547z"
                          />
                        </svg>
                      </label>
                    </div>
                  {/* Mobile menu button */}
                  <Disclosure.Button className="bg-gray-800 inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-white hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white">
                  
                    <span className="sr-only">Open main menu</span>
                    {open ? (
                      <XIcon className="block h-6 w-6" aria-hidden="true" />
                    ) : (
                      <MenuIcon className="block h-6 w-6" aria-hidden="true" />
                    )}
                  </Disclosure.Button>
                </div>
              </div>
            </div>

            <Disclosure.Panel className="md:hidden">
              <div className="px-2 pt-2 pb-3 space-y-1 sm:px-3">
                {navigation.map((item, itemIdx) =>
                  itemIdx === 0 ? (
                    <Fragment key={item}>
                      <a
                        href="#"
                        className="text-gray-300 hover:bg-gray-700 hover:text-white block px-3 py-2 rounded-md text-base font-medium"
                      >
                        {item}
                      </a>
                    </Fragment>
                  ) : (
                    <a
                      key={item}
                      href="#"
                      className="text-gray-300 hover:bg-gray-700 hover:text-white block px-3 py-2 rounded-md text-base font-medium"
                    >
                      {item}
                    </a>
                  )
                )}
              </div>
              <div className="pt-4 pb-3 border-t border-gray-700">
                <div className="flex  flex-col px-2">
                {/* <div className="flex  w-full px-4 ">
                      <ThemeToggle />
                      <label
                        for="toggle"
                        class="text-xs text-gray-700 dark:text-yellow-500"
                      >
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          className="h-6 w-6"
                          fill="none"
                          viewBox="0 0 24 24"
                          stroke="currentColor"
                        >
                          <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            strokeWidth={2}
                            d="M9.663 17h4.673M12 3v1m6.364 1.636l-.707.707M21 12h-1M4 12H3m3.343-5.657l-.707-.707m2.828 9.9a5 5 0 117.072 0l-.548.547A3.374 3.374 0 0014 18.469V19a2 2 0 11-4 0v-.531c0-.895-.356-1.754-.988-2.386l-.548-.547z"
                          />
                        </svg>
                      </label>
                    </div> */}
                    <div className="px-2 mt-1 text-center">
                    <button class="bg-blue-700 hover:bg-blue-700 text-gray-50 hover:text-gray-50 font-medium py-4 px-6 rounded flex w-full justify-center items-center text-center">
                    Get in touch
                    <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 ml-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
  <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M8 12h.01M12 12h.01M16 12h.01M21 12c0 4.418-4.03 8-9 8a9.863 9.863 0 01-4.255-.949L3 20l1.395-3.72C3.512 15.042 3 13.574 3 12c0-4.418 4.03-8 9-8s9 3.582 9 8z" />
</svg>
                  </button>
                    </div>
                </div>
              </div>
            </Disclosure.Panel>
          </>
        )}
      </Disclosure>

      <main className="max-w-screen-9xl mx-auto px-6 lg:px-8 lg:mt-40 mt-12">
        <div className="lg:mx-60 mx-20">
          <h1 className="font-header font-medium leading-tight lg:text-4xl text-2xl md:px-4 text-gray-800 dark:text-white">
            I am an experienced Digital Designer, bringing forth a motivated
            attitude and a variety of powerful skills. I am accustomed to
            working well with others and committed to meeting deadlines and
            adhering to project guidelines. I have a keen eye for quality and
            high standards, and I am a creative thinker with a strong
            multidisciplinary skillset.
          </h1>
        </div>
        <div className="grid lg:mt-40 md:mt-20 mt-12">
          <div className="relative rounded">
            <div class="">
              <img
                className="w-full rounded"
                src="https://drive.google.com/uc?export=view&id=1qH96STUXOjGqYYxVD-cmgaCSIUwR3sHy"
              ></img>
            </div>
            <Animated animationIn="bounceInLeft" animationOut="fadeOut" isVisible={true}>
            <div className="top-o left-0 right-0 bottom-0 opacity-0 hover:opacity-100 duration-300 flex-col flex absolute inset-0 z-10 justify-between items-center text-6xl text-black bg-yellow-500 dark:bg-yellow-100 font-semibold cursor-pointer transition duration-300 ease-in-out">
            
              <h3 className="text-sm pt-6 left-0 right-0">
              Retail Shopping App Concept
              </h3>
   
              <h2 className="text-4xl">Retail Shopping App Concept</h2>
              <h3 className="text-sm transform rotate-180 pt-6 left-0 right-0">
              Retail Shopping App Concept
              </h3>
            </div>
            </Animated>
          </div>
        </div>

        <div className="grid grid-cols-2 lg:gap-10 md:gap-5 lg:mt-10 md:mt-5 mt-5 gap-5">
          <div className="relative rounded">
            <div class="">
              <img
                className="w-full rounded"
                src="https://images.unsplash.com/photo-1525351326368-efbb5cb6814d?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=2000&q=80"
              ></img>
            </div>
            <div className="top-o left-0 right-0 bottom-0 opacity-0 hover:opacity-100 duration-300 flex-col flex absolute inset-0 z-10 justify-between items-center text-6xl sm:text-4xl text-black bg-purple-500 dark:bg-purple-100 font-semibold cursor-pointer transition duration-300 ease-in-out">
              <h3 className="text-xs md:text-sm pt-6 left-0 right-0">
              Retail Shopping App Concept
              </h3>
              <h2 className="md:text-4xl text-center text-xl">Retail Shopping App Concept</h2>
              <h3 className="text-xs md:text-sm transform rotate-180 pt-6 left-0 right-0">
              Retail Shopping App Concept
              </h3>
            </div>
          </div>

          <div className="relative rounded">
            <div class="">
              <img
                className="w-full rounded"
                src="https://images.unsplash.com/photo-1545366145-2c3ccd6fd902?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=80"
              ></img>
            </div>
            <div className="top-o left-0 right-0 bottom-0 opacity-0 hover:opacity-100 duration-300 flex-col flex absolute inset-0 z-10 justify-between items-center text-6xl text-black bg-green-500 dark:bg-green-100 font-semibold cursor-pointer transition duration-300 ease-in-out">
              <h3 className="text-xs md:text-sm pt-6 left-0 right-0">
              Retail Shopping App Concept
              </h3>
              <h2 className="md:text-4xl text-center text-xl">Retail Shopping App Concept</h2>
              <h3 className="text-xs md:text-sm transform rotate-180 pt-6 left-0 right-0">
              Retail Shopping App Concept
              </h3>
            </div>
          </div>
          
        </div>

        <div className="grid lg:mt-10 md:mt-20 mt-5">
          <div className="relative rounded">
            <div class="">
              <img
                className="w-full rounded"
                src="https://drive.google.com/uc?export=view&id=1qH96STUXOjGqYYxVD-cmgaCSIUwR3sHy"
              ></img>
            </div>
            <Animated animationIn="bounceInLeft" animationOut="fadeOut" isVisible={true}>
            <div className="top-o left-0 right-0 bottom-0 opacity-0 hover:opacity-100 duration-300 flex-col flex absolute inset-0 z-10 justify-between items-center text-6xl text-black bg-yellow-500 dark:bg-yellow-100 font-semibold cursor-pointer transition duration-300 ease-in-out">
            
              <h3 className="text-sm pt-6 left-0 right-0">
              Retail Shopping App Concept
              </h3>
   
              <h2 className="text-4xl">Retail Shopping App Concept</h2>
              <h3 className="text-sm transform rotate-180 pt-6 left-0 right-0">
              Retail Shopping App Concept
              </h3>
            </div>
            </Animated>
          </div>
        </div>

        <div className="flex justify-center items-center lg:mt-16 md:mt-8 mt-12">
          <a href="#"><button class=" dark:border-green-400 dark:text-green-400 dark:hover:bg-green-400 dark:hover:text-gray-50 border-2 border-blue-700 hover:bg-blue-700 text-blue-700 hover:text-gray-50 font-medium py-4 px-8 rounded flex ">
                    View all projects
                    <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 ml-2" fill="none" viewBox="0 0 24 24" stroke="currentColor">
  <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M10 6H6a2 2 0 00-2 2v10a2 2 0 002 2h10a2 2 0 002-2v-4M14 4h6m0 0v6m0-6L10 14" />
</svg>
                  </button></a>
          </div>
      </main>

      <footer>
        <div className="lg:mt-16 md:mt-8 h-16 mt-12 grid grid-cols-12 px-6 bg-gray-50 dark:bg-gray-700">
         <div className="col-span-8">
          <h2 className="text-3xl">get in touch</h2>
         </div>

         <div className="col-span-4">
          <h2 className="text-3xl">get in touch</h2>
         </div>
        </div>
      </footer>
    </div>
  );
}
