import React from "react"
import { ThemeToggler } from "gatsby-plugin-dark-mode"
export default function ThemeToggle() {
     return (
       <ThemeToggler>
        {({ theme, toggleTheme }) => (
          <div className="relative inline-block w-10 mr-2 align-middle select-none transition duration-200 ease-in">
             <input type="checkbox" name="toggle" id="toggle" className="toggle-checkbox absolute block w-6 h-6 rounded-full bg-white border-4 appearance-none cursor-pointer "
               onChange={e => 
                 toggleTheme(e.target.checked ? 'dark' : 'light')}
                  checked={theme === 'dark'}
               />{' '}

               <label for="toggle" class="toggle-label block overflow-hidden h-6 rounded-full bg-gray-300 cursor-pointer"></label>  
                     
          </div>
             
         
         )}
         
        </ThemeToggler>
     )
}

// <div class="relative inline-block w-10 mr-2 align-middle select-none transition duration-200 ease-in">
//     <input type="checkbox" name="toggle" id="toggle" class="toggle-checkbox absolute block w-6 h-6 rounded-full bg-white border-4 appearance-none cursor-pointer"/>
//     <label for="toggle" class="toggle-label block overflow-hidden h-6 rounded-full bg-gray-300 cursor-pointer"></label>
// </div>
